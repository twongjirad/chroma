# simple wrappers to avoid writing two sets of codes for CUDA and OpenCL
import numpy as np
import chroma.api as api
if api.is_gpu_api_opencl():
    import pyopencl as cl
    import pyopencl.array as ga
elif api.is_gpu_api_cuda():
    import pycuda.driver as cu
    from pycuda import gpuarray as ga

def to_device( nparray, queue=None ):
    if api.is_gpu_api_opencl():
        return ga.to_device( queue, nparray )
    elif api.is_gpu_api_cuda():
        return ga.to_gpu( nparray )

def empty( length, dtype=None, queue=None ):
    if api.is_gpu_api_opencl():
        return ga.empty( queue, length, dtype=dtype )
    elif api.is_gpu_api_cuda():
        return ga.empty( length, dtype=dtype )

def zeros( length, dtype=None, queue=None ):
    if api.is_gpu_api_opencl():
        return ga.zeros( queue, length, dtype=dtype )
    elif api.is_gpu_api_cuda():
        return ga.zeros( length, dtype=dtype )

